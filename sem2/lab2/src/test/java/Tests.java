import filesystem.Directory;
import filesystem.FileSystemNode;
import filesystem.exception.IncorrectPathException;
import filesystem.exception.NodeNotFoundException;
import filesystem.file.*;
import filesystem.path.Path;
import org.junit.jupiter.api.*;

import javax.naming.InvalidNameException;

import static org.junit.jupiter.api.Assertions.*;

public class Tests {
    Directory root;

    @BeforeEach
    void setup() throws InvalidNameException, NodeNotFoundException {
        this.root = new Directory("root");
        Directory dir = new Directory("Dir1");
        LogTextFile log = new LogTextFile("LogTextFile");
        log.input("Log Content");

        this.root.addChildNode(dir);
        this.root.addChildNode(log);
        this.root.addChildNode(new BinaryFile("BinaryFile", "Binary Content"));
        this.root.addChildNode(new BufferFile("BufferFile", 100));

        dir.addChildNode(new LogTextFile("LogFile2"));
        dir.addChildNode(new LogTextFile("LogFile3"));
    }

    @Test
    void getByPath() throws IncorrectPathException {
        FileSystemNode node = Path.getTargetByPath(this.root, "Dir1/LogFile2");
        assertEquals("LogFile2", node.getName());
    }

    @Test
    void getByIncorrectPath() {
        assertThrows(IncorrectPathException.class, () -> Path.getTargetByPath(this.root, "some/incorrect/path"));
    }

    @Test
    void getBySymbolicPath() throws IncorrectPathException {
        FileSystemNode node = Path.getTargetByPath(this.root, "./Dir1/..");
        assertEquals("root", node.getName());
    }

    @Test
    void getBinaryContent() throws IncorrectPathException {
        BinaryFile node = (BinaryFile)Path.getTargetByPath(this.root, "./BinaryFile");
        assertEquals("Binary Content", node.getContent());
    }

    @Test
    void changeBinaryContent() throws IncorrectPathException {
        BinaryFile node = (BinaryFile)Path.getTargetByPath(this.root, "./BinaryFile");
        node.input("SOME NEW STUFF");
        assertEquals("Binary Content", node.getContent());
    }

    @Test
    void getLogContent() throws IncorrectPathException {
        LogTextFile node = (LogTextFile)Path.getTargetByPath(this.root, "./LogTextFile");
        assertEquals("Log Content", node.getContent());
    }

    @Test
    void changeLogContent() throws IncorrectPathException {
        LogTextFile node = (LogTextFile)Path.getTargetByPath(this.root, "./LogTextFile");
        node.input(" SOME NEW STUFF");
        assertEquals("Log Content SOME NEW STUFF", node.getContent());
    }

    @Test
    void moveFile() throws IncorrectPathException, NodeNotFoundException {
        FileSystemNode node1 = Path.getTargetByPath(this.root, "./BufferFile");
        node1.moveTo("Dir1");
        FileSystemNode node2 = Path.getTargetByPath(this.root, "./Dir1/BufferFile");
        assertEquals(node1, node2);
    }

    @Test
    void moveFileBySymbolicLink() throws IncorrectPathException, NodeNotFoundException {
        FileSystemNode node1 = Path.getTargetByPath(this.root, "./Dir1/LogFile2");
        node1.moveTo("..");
        FileSystemNode node2 = Path.getTargetByPath(this.root, "./LogFile2");
        assertEquals(node1, node2);
    }
}
