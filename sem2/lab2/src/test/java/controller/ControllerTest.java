package controller;

import filesystem.controllers.Controller;
import filesystem.controllers.actions.*;
import filesystem.file.LogTextFile;
import org.junit.jupiter.api.*;

import java.util.concurrent.ExecutionException;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import filesystem.*;

import javax.naming.InvalidNameException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ControllerTest {
    Directory root;
    Controller<Directory> controller;
    Random rand;
    int numOfThreads = 4;

    ControllerTest() throws InvalidNameException {
        this.root = new Directory("root");
        this.controller = new Controller<>(root, numOfThreads);
        this.rand = new Random();
    }

    @Test
    void createDirsAndFiles() throws ExecutionException, InterruptedException {
        int numOfDirectories = 100;

        IntStream
            .range(0, numOfDirectories)
            .forEach(index -> {
                try {
                    controller.submit(
                            new Create(new Directory("dir_" + index))
                    ).get();
                } catch (InvalidNameException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });

        int count = this.controller.submit(new ACount(true)).get();
        assertEquals(numOfDirectories, count);

        IntStream
                .range(0, numOfDirectories)
                .forEach(index -> {
                    try {
                        Directory childDirectory = (Directory) this.controller.submit(
                                new Get("./dir_" + index)
                        ).get();
                        Controller<Directory> childDirectoryController = new Controller<>(childDirectory, numOfThreads);

                        IntStream
                                .range(0, numOfDirectories)
                                .forEach(subIndex -> {
                                    try {
                                        childDirectoryController.submit(new Create(
                                                new LogTextFile("file_" + subIndex)
                                        )).get();
                                    } catch (InvalidNameException e) {
                                        e.printStackTrace();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } catch (ExecutionException e) {
                                        e.printStackTrace();
                                    }
                                });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                });

        count = this.controller.submit(new ACount(true)).get();
        assertEquals(numOfDirectories * (numOfDirectories + 1), count);

        this.controller.submit(
                new AReplaceInName(
                        "11",
                        "eleventh",
                        true
                )
        ).get();

        count = this.controller.submit(new ASearch(".*_eleventh")).get().size();
        assertEquals(numOfDirectories + 1, count);
    }

    @Test
    void generateTree() throws InvalidNameException, ExecutionException, InterruptedException {
        this.controller.submit(
               new Create(new Directory("dir"))
        ).get();
        Directory childDirectory = (Directory) this.controller.submit(
                new Get("./dir")
        ).get();
        Controller<Directory> childDirectoryController = new Controller<>(childDirectory, numOfThreads);
        childDirectoryController.submit(
                new Create(new LogTextFile("file"))
        ).get();

        String actualTree = this.controller.submit(new ATree()).get();
        String expectedTree = "{\"name\":\"root\",\"files\":[],\"directories\":[{\"name\":\"dir\",\"files\":[{\"name\":\"file\",\"type\":\"LogTextFile\",\"content\":\"\"}],\"directories\":[]}]}";
        assertEquals(expectedTree, actualTree);
    }
}
