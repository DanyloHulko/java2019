package filesystem;

import filesystem.exception.IncorrectPathException;
import filesystem.exception.NodeNotFoundException;
import filesystem.locker.ReadWriteLocker;
import filesystem.path.Path;

import javax.naming.InvalidNameException;

public abstract class FileSystemNode {
    protected String name;
    protected Directory parentNode;
    protected ReadWriteLocker locker;

    public FileSystemNode(String name) throws InvalidNameException {
        this.locker = new ReadWriteLocker();
        this.setName(name);
    }

    public void setName(String newName) throws InvalidNameException {
        //TODO newName validation
        this.locker.write(() -> this.name = newName);
    }

    public String getName() {
        return this.locker.read(() -> this.name);
    }

    public Directory getParentNode() {
        return this.locker.read(() -> this.parentNode);
    }

    public void moveTo(String path) throws IncorrectPathException, NodeNotFoundException {
        Directory parent = this.getParentNode();
        FileSystemNode targetNode = Path.getTargetByPath(parent, path);
        if(targetNode instanceof Directory) {
            Directory targetDirectory = (Directory) targetNode;
            targetDirectory.addChildNode(
                    parent.removeChildNode(this)
            );
        }
    }

    public FileSystemNode removeSelf() throws NodeNotFoundException {
        return this.getParentNode().removeChildNode(this);
    }
}
