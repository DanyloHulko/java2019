package filesystem.exception;

public class IncorrectFileNameException extends Exception {
    public IncorrectFileNameException(String fileName) {
        super(String.format(
                "Incorrect filename < %s >",
                fileName
        ));
    }
}
