package filesystem.controllers;

import filesystem.FileSystemNode;
import filesystem.controllers.actions.FSAction;

import java.util.concurrent.*;

public class Controller<N extends FileSystemNode> {
    ExecutorService workers;
    N node;

    public Controller(N node, int numOfThreadsToExecute) {
        this.node = node;
        this.workers = Executors.newFixedThreadPool(numOfThreadsToExecute);
    }

    public<T> Future<T> submit(FSAction<N, T> action) {
        return this.workers.submit(() -> action.call(this.node));
    }

}
