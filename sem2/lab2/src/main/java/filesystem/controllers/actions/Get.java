package filesystem.controllers.actions;

import filesystem.Directory;
import filesystem.FileSystemNode;
import filesystem.path.Path;

public class Get implements FSAction<Directory, FileSystemNode> {
    String path;

    public Get(String path){
        this.path = path;
    }

    public FileSystemNode call(Directory node) {
        try {
            return Path.getTargetByPath(node, this.path);
        }
        catch (Exception e) {
            return null;
        }
    }
}
