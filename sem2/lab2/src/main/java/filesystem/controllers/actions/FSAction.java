package filesystem.controllers.actions;

import filesystem.FileSystemNode;

public interface FSAction<I extends FileSystemNode, O> {
    O call(I node);
}
