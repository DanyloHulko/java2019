package filesystem.controllers.actions;

import filesystem.Directory;
import filesystem.FileSystemNode;
import filesystem.path.Path;

public class Move implements FSAction<FileSystemNode, Void> {
    String path;

    public Move(String path){
        this.path = path;
    }

    public Void call(FileSystemNode node) {
        //TODO: Probably, the exception should be logged
        try {
            node.moveTo(this.path);
        }
        catch (Exception e) { }
        return null;

    }
}