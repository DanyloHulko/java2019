package filesystem.controllers.actions;

import filesystem.Directory;
import filesystem.jobs.ReplaceInName;

import java.util.concurrent.ForkJoinPool;

public class AReplaceInName implements FSAction<Directory, Void> {
    boolean shouldRunRecursively;
    String pattern;
    String replacement;

    public AReplaceInName(String pattern, String replacement, boolean shouldRunRecursively) {
        this.pattern = pattern;
        this.replacement = replacement;
        this.shouldRunRecursively = shouldRunRecursively;
    }
    @Override
    public Void call(Directory node) {
        return ForkJoinPool.commonPool()
                .submit(new ReplaceInName(node, this.pattern, this.replacement, this.shouldRunRecursively))
                .join();
    }
}