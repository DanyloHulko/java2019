package filesystem.controllers.actions;

import filesystem.Directory;
import filesystem.jobs.Count;

import java.util.concurrent.ForkJoinPool;

public class ACount implements FSAction<Directory, Integer> {
    boolean shouldRunRecursively;

    public ACount(boolean shouldRunRecursively) {
        this.shouldRunRecursively = shouldRunRecursively;
    }
    @Override
    public Integer call(Directory node) {
        return ForkJoinPool.commonPool()
                .submit(new Count(node, this.shouldRunRecursively))
                .join();
    }
}
