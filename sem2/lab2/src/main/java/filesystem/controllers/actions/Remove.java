package filesystem.controllers.actions;

import filesystem.FileSystemNode;

public class Remove implements FSAction<FileSystemNode, Void> {
    public Void call(FileSystemNode node) {
        //TODO: Probably, the exception should be logged
        try {
            node.removeSelf();
        }
        catch (Exception e) { }
        return null;

    }
}