package filesystem.controllers.actions;

import filesystem.file.File;

public class Input implements FSAction<File, Void> {
    String content;

    public Input(String content) {
        this.content = content;
    }

    @Override
    public Void call(File node) {
        node.input(this.content);
        return null;
    }
}
