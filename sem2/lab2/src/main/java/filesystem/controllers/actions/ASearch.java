package filesystem.controllers.actions;

import filesystem.Directory;
import filesystem.FileSystemNode;
import filesystem.jobs.Search;

import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class ASearch implements FSAction<Directory, List<FileSystemNode>> {
    String pattern;

    public ASearch(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public List<FileSystemNode> call(Directory node) {
        return ForkJoinPool.commonPool()
                .submit(new Search(node, this.pattern))
                .join();
    }
}
