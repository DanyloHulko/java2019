package filesystem.controllers.actions;

import filesystem.Directory;
import filesystem.jobs.Tree;

import java.util.concurrent.ForkJoinPool;

public class ATree implements FSAction<Directory, String> {
    @Override
    public String call(Directory node) {
        return ForkJoinPool.commonPool()
                .submit(new Tree(node))
                .join()
                .toJSONString();
    }
}
