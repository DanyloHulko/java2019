package filesystem.controllers.actions;

import filesystem.Directory;
import filesystem.FileSystemNode;

public class Create implements FSAction<Directory, Void> {
    FileSystemNode nodeToAdd;

    public Create(FileSystemNode nodeToAdd) {
        this.nodeToAdd = nodeToAdd;
    }

    @Override
    public Void call(Directory node) {
        try{
            node.addChildNode(this.nodeToAdd);
        }
        catch (Exception e) { }
        return null;
    }
}
