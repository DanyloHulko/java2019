package filesystem.jobs;


import filesystem.Directory;
import filesystem.FileSystemNode;

import javax.naming.InvalidNameException;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

//method replace substring with fixed string in the names of all nodes inside current node recursively.
public class ReplaceInName extends RecursiveTask<Void> {
    Directory directory;
    boolean shouldRunRecursively;
    String pattern;
    String replacement;


    public ReplaceInName(Directory directory, String pattern, String replacement, boolean shouldRunRecursively) {
        this.directory = directory;
        this.pattern = pattern;
        this.replacement = replacement;
        this.shouldRunRecursively = shouldRunRecursively;
    }

    @Override
    public Void compute() {
        List<FileSystemNode> childNodes = this.directory.getChildNodes();
        this.directory.getChildNodes().forEach(node -> {
            try {
                node.setName(
                        node.getName().replaceAll(this.pattern, this.replacement)
                );
            } catch (InvalidNameException e) {
                e.printStackTrace();
            }
        });
        if(shouldRunRecursively) {
            //Fork
            List<ForkJoinTask<Void>> taskList = childNodes.stream()
                    .filter(node -> node instanceof Directory)
                    .map(node -> new ReplaceInName(
                            (Directory)node,
                            this.pattern,
                            this.replacement,
                            true
                    ).fork())
                    .collect(Collectors.toList());
            //Join
            for(ForkJoinTask<Void> task : taskList) {
                task.join();
            }
        }
        return null;
    }
}
