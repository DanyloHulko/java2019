package filesystem.jobs;

import filesystem.Directory;
import filesystem.FileSystemNode;

import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

public class Search extends RecursiveTask<List<FileSystemNode>> {
    String pattern;
    Directory directory;

    public Search(Directory directory, String pattern) {
        this.pattern = pattern;
        this.directory = directory;
    }

    @Override
    public List<FileSystemNode> compute() {
        List<FileSystemNode> childNodes = this.directory.getChildNodes();
        //Fork
        List<ForkJoinTask<List<FileSystemNode>>> taskList = childNodes.stream()
                .filter(node -> node instanceof Directory)
                .map(node -> new Search(
                        (Directory)node,
                        this.pattern
                ).fork())
                .collect(Collectors.toList());
        //Execute
        List<FileSystemNode> result = childNodes.stream()
                .filter(node -> node.getName().matches(this.pattern))
                .collect(Collectors.toList());
        //Join
        taskList.forEach(task -> result.addAll(task.join()));
        return result;
    }
}

