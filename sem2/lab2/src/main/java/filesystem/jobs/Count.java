package filesystem.jobs;

import filesystem.Directory;
import filesystem.FileSystemNode;

import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Count extends RecursiveTask<Integer> {
    Directory directory;
    boolean shouldRunRecursively;

    public Count(Directory directory, boolean shouldRunRecursively) {
        this.shouldRunRecursively = shouldRunRecursively;
        this.directory = directory;
    }

    @Override
    public Integer compute() {
        List<FileSystemNode> childNodes = this.directory.getChildNodes();
        Integer result = this.directory.getChildNodes().size();
        if(shouldRunRecursively) {
            //Fork
            List<ForkJoinTask<Integer>> taskList = childNodes.stream()
                    .filter(node -> node instanceof Directory)
                    .map(node -> new Count(
                            (Directory)node,
                            true
                    ).fork())
                    .collect(Collectors.toList());
            //Join
            for(ForkJoinTask<Integer> task : taskList) {
                result += task.join();
            }
        }
        return result;
    }
}