package filesystem.jobs;

import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

import filesystem.Directory;
import filesystem.FileSystemNode;
import filesystem.file.File;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Tree extends RecursiveTask<JSONObject> {
    Directory directory;

    public Tree(Directory directory) {
        this.directory = directory;
    }

    private JSONObject convertFileToJSON(File file) {
        JSONObject result = new JSONObject();
        result.put("name", file.getName());
        result.put("type", file.getClass().getSimpleName());
        result.put("content", file.getContent());
        return result;
    }

    @Override
    public JSONObject compute() {
        List<FileSystemNode> childNodes = this.directory.getChildNodes();
        //Fork
        List<ForkJoinTask<JSONObject>> taskList = childNodes.stream()
                .filter(node -> node instanceof Directory)
                .map(node -> new Tree(
                        (Directory)node
                ).fork())
                .collect(Collectors.toList());
        //Execute
        JSONObject result = new JSONObject();
        result.put("name", this.directory.getName());

        JSONArray files = new JSONArray();
        childNodes.stream()
                .filter(node -> node instanceof File)
                .forEach(node -> files.add(
                        convertFileToJSON((File) node)
                ));
        result.put("files", files);

        JSONArray directories = new JSONArray();
        taskList.forEach(task -> directories.add(task.join()));
        result.put("directories", directories);

        return result;
    }
}
