package filesystem.file;

import filesystem.FileSystemNode;

import javax.naming.InvalidNameException;

public abstract class File extends FileSystemNode {
    protected String content;

    public File(String name) throws InvalidNameException {
        super(name);
    }

    public String getContent() {
        return this.locker.read(() -> this.content);
    }

    protected void setContent(String content) {
        this.locker.write(() -> this.content = content);
    }

    public abstract void input(String content);
}
