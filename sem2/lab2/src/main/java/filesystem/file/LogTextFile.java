package filesystem.file;

import javax.naming.InvalidNameException;

public class LogTextFile extends File {
    public LogTextFile(String name) throws InvalidNameException {
        super(name);
        this.setContent("");
    }

    @Override
    public void input(String input) {
        String currentContent = this.getContent();
        this.setContent(currentContent.concat(input));
    }
}