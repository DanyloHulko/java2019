import filesystem.Directory;
import filesystem.file.BinaryFile;
import filesystem.file.BufferFile;
import filesystem.file.LogTextFile;
import runners.*;
import stringify.Stringify;

import java.util.concurrent.CyclicBarrier;

public class Main {
    public static void main(String[] args) {
        try{
            System.out.println("--------------------TASK2---------------------");
            Directory root = new Directory("root");
            Directory nestedDir = new Directory("nested_lvl1");
            Directory nestedDir2 = new Directory("nested_lvl2");
            root.addChildNode(nestedDir);
            nestedDir.addChildNode(new BinaryFile("binary", "bin_content"));
            nestedDir.addChildNode(nestedDir2);
            nestedDir2.addChildNode(new LogTextFile("log"));
            root.addChildNode(new BufferFile("buffer", 1000));
            System.out.println(root.getChildNode("nested_lvl1").getName());
            System.out.println(Stringify.fs(root));

            System.out.println("----------------------------------------------");
            System.out.println("--------------------TASK3---------------------");

            CyclicBarrier cyclicBarrier = new CyclicBarrier(6);
            new Thread(new TaskRunner1(cyclicBarrier), "T1").start();
            new Thread(new TaskRunner2(cyclicBarrier), "T2").start();
            new Thread(new TaskRunner3(cyclicBarrier), "T3").start();
            new Thread(new TaskRunner4(cyclicBarrier), "T4").start();
            new Thread(new TaskRunner5(cyclicBarrier), "T5").start();

            //Task3 and Task3
            cyclicBarrier.await();
            //Task1 and Task2
            cyclicBarrier.await();
            //Task5

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
