package filesystem;

import filesystem.exception.NodeNotFoundException;

import javax.naming.InvalidNameException;
import java.util.ArrayList;
import java.util.List;

public class Directory  extends FileSystemNode {
    List<FileSystemNode> childNodes;

    public Directory(String name) throws InvalidNameException {
        super(name);
        childNodes = new ArrayList<>();
    }

    public List<FileSystemNode> getChildNodes() {
        return this.locker.read(() -> childNodes);
    }

    public FileSystemNode getChildNode(String name) {
        return this.locker.read(() ->
                childNodes.stream()
                    .filter((FileSystemNode node) -> node.getName().equals(name))
                    .findFirst()
                    .orElse(null)
        );
    }

    public void addChildNode(FileSystemNode node) throws NodeNotFoundException {
        if(node != null) {
            this.locker.write(() -> {
                node.parentNode = this;
                this.childNodes.add(node);
            });
        }
        else {
            throw new NodeNotFoundException("Cannot add null node");
        }
    }

    public FileSystemNode removeChildNode(FileSystemNode node) throws NodeNotFoundException {
        if(node != null) {
            FileSystemNode removedNode =  this.locker.readWrite(() -> {
                if(node.parentNode == this) {
                    node.parentNode = null;
                    this.childNodes.remove(node);
                    return node;
                }
                else {
                    return null;
                }
            });
            if(removedNode == null) {
                throw new NodeNotFoundException(node.getName(), this.getName());
            }
            return removedNode;
        }
        else {
            throw new NodeNotFoundException("Cannot remove null node");
        }
    }

    public FileSystemNode removeChildNode(String nodeName) throws Exception {
        return this.removeChildNode(this.getChildNode(nodeName));
    }

}
