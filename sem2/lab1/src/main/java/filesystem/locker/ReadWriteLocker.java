package filesystem.locker;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

public class ReadWriteLocker {
    ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public  <T> T read(Supplier<T> supplier) {
        readWriteLock.readLock().lock();
        T instance = supplier.get();
        readWriteLock.readLock().unlock();
        return instance;
    }

    public void write(Runnable runner) {
        readWriteLock.writeLock().lock();
        runner.run();
        readWriteLock.writeLock().unlock();
    }

    public <T> T readWrite(Supplier<T> supplier) {
        readWriteLock.writeLock().lock();
        T res = supplier.get();
        readWriteLock.writeLock().unlock();
        return res;
    }


}
