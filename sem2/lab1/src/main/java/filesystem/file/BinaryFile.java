package filesystem.file;

import javax.naming.InvalidNameException;

public class BinaryFile extends File {
    public BinaryFile(String name, String content) throws InvalidNameException {
        super(name);
        this.setContent(content);
    }

    @Override
    public void input(String content) {
        /* input should be ignored */
    }
}
