package filesystem.file;

import javax.naming.InvalidNameException;

public class BufferFile extends File {
    private int maxNumberOfCharacters;

    public BufferFile(String name, int maxNumberOfCharacters) throws InvalidNameException {
        super(name);
        this.maxNumberOfCharacters = maxNumberOfCharacters;
        this.setContent("");
    }

    @Override
    public void input(String input) {
        String currentContent = this.getContent();
        String contentWithInput = currentContent.concat(input);
        if(contentWithInput.length() > this.maxNumberOfCharacters) {
            contentWithInput = contentWithInput.substring(0, this.maxNumberOfCharacters);
        }
        this.setContent(contentWithInput);
    }

    public String take(int requestedNumberOfCharacters) {
        return this.locker.readWrite(() -> {
            int numberOfCharactersToRead = Math.min(requestedNumberOfCharacters, this.content.length());
            String result = this.content.substring(0, numberOfCharactersToRead);
            String updatedContent = this.content.substring(numberOfCharactersToRead, this.content.length());

            //write content, read content
            this.content = updatedContent;
            return result;
        });

    }
}
