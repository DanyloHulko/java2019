package filesystem.path;

import filesystem.Directory;
import filesystem.FileSystemNode;
import filesystem.exception.IncorrectPathException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class Path {
    /*
    * Path example:
    *   root/dir1/dir2/file - hard link
    *   ./../siblingDir/file - soft link
    * */
    static private String nodeDelimiter = "/";
    static private String currentDirectorySymbol = ".";
    static private String parentDirectorySymbol = "..";

    private static boolean isSymbolic(Stack<String> path) {
        if(path == null || path.isEmpty())
            return false;
        String firstNodeName = path.peek();
        return firstNodeName.equals(currentDirectorySymbol)
                || firstNodeName.equals(parentDirectorySymbol);
    }

    public static Stack<String> split(String path) {
        Stack<String> result = new Stack<>();
        List<String> nodeList = Arrays.stream(path.split(nodeDelimiter)).collect(Collectors.toList());
        Collections.reverse(nodeList);
        result.addAll(nodeList);
        return result;
    }

    public static Directory getRootDirectory(Directory currentDirectory) {
        Directory iterator = currentDirectory;
        while(iterator != null && iterator.getParentNode() != null) {
            iterator = iterator.getParentNode();
        }
        return iterator;
    }

    public static FileSystemNode getTargetByPath(Directory currentDirectory, String path) throws IncorrectPathException {
        return Path.getTargetByPath(currentDirectory, Path.split((path)));
    }



    private static FileSystemNode getNextNode(Directory currentDirectory, String nextNodeName) {
        return currentDirectory == null || nextNodeName.equals(currentDirectorySymbol)
                ? currentDirectory
                : nextNodeName.equals(parentDirectorySymbol)
                    ? currentDirectory.getParentNode()
                    : currentDirectory.getChildNode(nextNodeName);
    }

    public static FileSystemNode getTargetByPath(Directory currentDirectory, Stack<String> path) throws IncorrectPathException {

        //we should not change received stack. It should be copied
        Stack<String> iteratedPath = (Stack<String>)path.clone();

        Directory iteratedDirectory = Path.isSymbolic(iteratedPath)
                ? currentDirectory
                : Path.getRootDirectory(currentDirectory);
        while(!iteratedPath.isEmpty()) {
            FileSystemNode node = getNextNode(iteratedDirectory, iteratedPath.pop());
            if(node == null) {
                throw new IncorrectPathException(path.toString());
            }
            if(iteratedPath.isEmpty()) {
                return node;
            }
            if(!(node instanceof Directory)) {
                throw new IncorrectPathException(path.toString(), node.getName());
            }

            iteratedDirectory = (Directory)node;
        }
        return iteratedDirectory;
    }
}
