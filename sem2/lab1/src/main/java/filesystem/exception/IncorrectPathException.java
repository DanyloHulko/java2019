package filesystem.exception;

public class IncorrectPathException extends Exception {
    public IncorrectPathException(String path) {
        super(
                String.format("The path < %s > is not exist", path)
        );
    }

    public IncorrectPathException(String path, String fileName) {
        this(path, fileName, "");
    }

    public IncorrectPathException(String path, String fileName, String additionalMessage) {
        super(
                String.format("In path < %s > the link %s is file and not a directory. %s",
                        path,
                        fileName,
                        additionalMessage
                )
        );
    }
}
