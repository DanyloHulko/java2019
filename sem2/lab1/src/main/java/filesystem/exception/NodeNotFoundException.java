package filesystem.exception;

public class NodeNotFoundException extends Exception {
    public NodeNotFoundException(String nodeName, String parentName) {
        super(String.format("Cannot find any nodes with name < %s > in the < %s > directory",
                nodeName,
                parentName
        ));
    }

    public NodeNotFoundException(String message) {
        super(message);
    }
}
