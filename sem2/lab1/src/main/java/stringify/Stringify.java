package stringify;

import filesystem.Directory;
import filesystem.FileSystemNode;

import java.util.List;

public class Stringify {
    private static String getSpaces(int level) {
        String space = "";
        for(int i = 0; i < level; i++)
            space += "  ";
        return space;
    }

    private static String printLine(FileSystemNode fileSystemNode, int level) {
        return String.format("%s%s\n", getSpaces(level), fileSystemNode.getName());
    }

    private static String getRootTree(Directory dir, int level) {
        String result = printLine(dir, level);
        List<FileSystemNode> childNodes = dir.getChildNodes();
        for(FileSystemNode childNode : childNodes) {
            if(childNode instanceof Directory) {
                result += getRootTree((Directory) childNode, level + 1);
            }
            else {
                result += printLine(childNode, level + 1);
            }
        }
        return result;
    }

    public static String fs(Directory root) {
        return getRootTree(root, 0);
    }
}
