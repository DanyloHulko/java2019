package runners;

import filesystem.Directory;

import java.util.concurrent.CyclicBarrier;

public class TaskRunner3 extends TaskRunner {

    public TaskRunner3(CyclicBarrier cyclicBarrier) {
        super(cyclicBarrier);
    }

    @Override
    public void task_finalize() {
        super.task_finalize();
        try{
            this.cyclicBarrier.await();
            this.cyclicBarrier.await();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
