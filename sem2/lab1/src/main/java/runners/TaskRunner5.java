package runners;

import filesystem.Directory;
import filesystem.file.LogTextFile;
import filesystem.path.Path;

import java.util.concurrent.CyclicBarrier;

public class TaskRunner5 extends TaskRunner {
    public TaskRunner5(CyclicBarrier cyclicBarrier) {
        super(cyclicBarrier);
    }

    @Override
    public void task_init() {
        try{
            this.cyclicBarrier.await();
            this.cyclicBarrier.await();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
        super.task_init();
    }
}
