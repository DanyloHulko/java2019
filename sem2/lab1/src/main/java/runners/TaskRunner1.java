package runners;

import filesystem.Directory;
import filesystem.file.LogTextFile;
import filesystem.path.Path;

import java.util.concurrent.CyclicBarrier;

public class TaskRunner1 extends TaskRunner {
    public TaskRunner1(CyclicBarrier cyclicBarrier) {
        super(cyclicBarrier);
    }

    @Override
    public void task() {
        super.task();
        try{
            this.cyclicBarrier.await();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    @Override
    public void task_finalize() {
        super.task_finalize();
        try{
            this.cyclicBarrier.await();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
