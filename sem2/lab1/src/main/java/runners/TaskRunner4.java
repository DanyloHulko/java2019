package runners;

import java.util.concurrent.CyclicBarrier;

public class TaskRunner4 extends TaskRunner {
    public TaskRunner4(CyclicBarrier cyclicBarrier) {
        super(cyclicBarrier);
    }

    @Override
    public void task_finalize() {
        super.task_finalize();
        try{
            this.cyclicBarrier.await();
            this.cyclicBarrier.await();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
