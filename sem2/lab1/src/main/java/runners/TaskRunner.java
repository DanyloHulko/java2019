package runners;

import filesystem.Directory;
import java.util.concurrent.CyclicBarrier;

public class TaskRunner implements Runnable {
    Directory root;
    protected CyclicBarrier cyclicBarrier;

    public TaskRunner(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        this.task_init();
        this.task();
        this.task_finalize();
    }

    public void task_init() {
        System.out.println("task_init() in the " + this.getClass());
    }

    public void task() {
        System.out.println("task() in the " + this.getClass());
    }

    public void task_finalize() {
        System.out.println("task_finalize() in the " + this.getClass());
    }
}
